## Inspiration
Hajj event is one of the holiest ritual events in Islam. 2M+ people gather in one place at the same time, and the same place and managing such a thing is very challenging and might result in very catastrophic problems if not being managed well. One of the key solutions to most of the challenges is to have applications that solve problems with very responsive way and to be scalable to host more pilgrims over 4M+ to allow many more people to do this ritual with the use of crowd and volunteers alongside with the government people things will go much easier and more manageable.

## What it does
SANED is generally a location-based SOS application the makes it easy to the pilgrims to ask for an emergency medical/security help, location directions, special needs/services, and many other help types which a pilgrim may need during his pilgrimage experience and to connect them with people from different Hajj managements and crowdsourcing volunteers.

## How we built it
Swift for iOS mobile application and Backend using PHP language with Laravel framework to build the dashboard and Lumen to build the API. Where every feature operates with a stand-alone microservice from day one.

## Challenges we ran into
- The unease of ambulance staff and the red cross to reach to the pilgrims in Hajj because of the congestion
- Motivate pilgrims to help each other
- Make the process of contacting supervisors and reaching to the specific management personnel easily and fast.
- The help of special needs anywhere and time.
- Responsiveness helping in congestion

## Accomplishments that we're proud of
- Make SANED all in one essential application for pilgrims help
- Make it scalable from day 1
- Make it crowdsourcing
- An innovative way of verification

## What's next for SANED
- Gemify volunteering and add padges, rating, and prizes.
- Providing driving directions for whoever will help.
- Push notifications and SMS
- Adding more features that can accept the help of the crowd.
- Add augmented reality to some of the application features including helping in navigation.
- Add two way SMS for offline messaging between pilgrims and pilgrims helping committee.
- Integrate with drones to go and capture video feed from the SOS location and decide if it is a valid SOS or false alarm.